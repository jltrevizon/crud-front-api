import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject, tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MasterService {

  private _refreshrequired = new Subject<void>();

  get Refreshrequired() {
    return this._refreshrequired;
  }

  constructor(private http: HttpClient) { }

  apiurl = "http://localhost:8080/api/item";

  getItem() {
    return this.http.get(this.apiurl);
  }

  saveItem(inputData: any) {
    if (inputData.id == 0){
      return this.http.post(this.apiurl, inputData).pipe(
        tap(() => {
          this._refreshrequired.next();
        })
      );
    } else {
      return this.http.put(this.apiurl, inputData).pipe(
        tap(() => {
          this._refreshrequired.next();
        })
      );
    }

  }

  getItemById(id: any) {
    return this.http.get(this.apiurl + '/' + id);
  }

 
  removeItem(id: any) {
    return this.http.delete(this.apiurl + '/' + id);
  }

}
