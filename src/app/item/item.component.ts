import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { PopupComponent } from '../popup/popup.component';
import { MasterService } from '../service/master.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements AfterViewInit {

  constructor(private service: MasterService) {
    this.getItem();

    this.service.Refreshrequired.subscribe(result=>{
      this.getItem();
    });

  }

  @ViewChild(PopupComponent) addview !:PopupComponent

  itemList: any;

  getItem() {
    this.service.getItem().subscribe(result => {
      this.itemList = result;
    });
  }

  edit(id:any){
    this.addview.loadEditData(id);
  }


  delete(id:any){
    if(confirm("Eliminar item #"+id+"?")){
      this.service.removeItem(id).subscribe(result => {
        this.getItem();
      });
    }

  }

  ngAfterViewInit(): void {

  }

}
