import { Component, ViewChild, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MasterService } from '../service/master.service';



@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent implements OnInit {

  packaging_type = ['Botella', 'Caja']
  error_message = '';
  error_class = '';
  save_response: any;
  edit_data: any;

  itemForm = new FormGroup({
    id: new FormControl({value: 0, disabled: true }),
    name: new FormControl('', Validators.compose([Validators.required, Validators.minLength(5)])),
    requireFridge: new FormControl(false, Validators.compose([Validators.required])),
    capacity: new FormControl(0, Validators.compose([Validators.required])),
    packagingType: new FormControl('', Validators.compose([Validators.required])),
  });


  constructor(private modalService: NgbModal, private service: MasterService) { }

  @ViewChild('content') addview !: ElementRef


  ngOnInit(): void {

  }


  saveItem() {
    if (this.itemForm.valid) {

      console.log(this.itemForm.getRawValue());

      this.service.saveItem(this.itemForm.getRawValue()).subscribe(result => {
        this.save_response = result;
          this.error_message = "Saved Sucessfully";
          this.error_class = "sucessmessage";
          setTimeout(()=>{
            this.modalService.dismissAll();
            this.error_message = '';
            this.error_class = '';
          },1000)
      }, (e)=>{
        this.error_message = "Failed to save";
        this.error_class = "errormessage";
      });

    } else {
      this.error_message = "Please enter valid data";
      this.error_class = "errormessage";
    }
  }


  loadEditData(id: any) {
    this.error_message = '';
    this.error_class = '';

    this.open();
    this.service.getItemById(id).subscribe(result => {
      this.edit_data = result;
      this.itemForm.setValue({
        id:this.edit_data.id,
        name:this.edit_data.name,
        requireFridge:this.edit_data.requireFridge,
        capacity:this.edit_data.capacity,
        packagingType:this.edit_data.packagingType
      });

    });



  }

  clearForm(){
    this.itemForm.setValue({id:0,name:'',requireFridge:false,capacity:0,packagingType:''})
  }

  open() {
    this.clearForm();
    this.modalService.open(this.addview, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
    }, (reason) => {
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  get fName() {
    return this.itemForm.get("name");
  }
  get fRequireFridge() {
    return this.itemForm.get("requireFridge");
  }
  get fCapacity() {
    return this.itemForm.get("capacity");
  }
  get fPackagingType() {
    return this.itemForm.get("packagingType");
  }



}
